import os
from string import Template


__all__ = ['makeMainExpression', 'makeStereoExpression']

curdir = os.path.dirname(__file__)

# reading mainExpression from file
mainExpressionString = ''
mainExpressionFileName = os.path.join(curdir, 'mainExpression.mel')
with open(mainExpressionFileName) as mainExpressionFile:
    mainExpressionString = mainExpressionFile.read()
mainExpressionTemplate = Template(mainExpressionString)

def makeMainExpression(p, cameraScale, hAperture, vAperture, focalLength, loctz, locsx, locsy):
    variables = {
            's_p': str(p),
            's_cameraScale': str(cameraScale),
            's_horizontalAperture': str(hAperture),
            's_verticalAperture': str(vAperture),
            's_mainCamShapeFocalLength': str(focalLength),
            's_loc2TranslateZ': str(loctz),
            's_loc2ScaleX': str(locsx),
            's_loc2ScaleY': str(locsy),
            }
    return mainExpressionTemplate.safe_substitute(variables)


# reading stereoExpression from file
stereoExpressionString = ''
stereoExpressionFileName = os.path.join(curdir, 'stereoExpression.mel')
with open(stereoExpressionFileName) as stereoExpressionFile:
    stereoExpressionString = stereoExpressionFile.read()
stereoExpressionTemplate = Template(stereoExpressionString)

def makeStereoExpression(nCams, p, hAperture, vAperture, cameraScale, camIndex, camTranslateX,
        filmOffset, focalLength):
    variables = {
            's_nCams': str(nCams),
            's_p': str(p),
            's_horizontalAperture': str(hAperture),
            's_verticalAperture': str(vAperture),
            's_cameraScale': str(cameraScale),
            's_camIndex': str(camIndex),
            's_camTranslateX': str(camTranslateX),
            's_camShapeHorizontalFilmOffset': str(filmOffset),
            's_camShapeFocalLength': str(focalLength)
            }
    return stereoExpressionTemplate.safe_substitute(variables)
