# What is it #

This script enables creation of a stereoscopic camera rig constrained according to the rules outlined by [alioscopy](http://www.alioscopy.com)

## Setup

1. unpack the repository in your site directory in a directory named `alioscopy`
2. Then within maya script editor enter the following

```
#!python

import alioscopy
alioscopy.makeCams()
```